(defproject stigmergy/chp "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [http-kit "2.3.0"]
                 [ring/ring-core "1.7.1"]
                 [bidi "2.1.4"]
                 [garden "1.3.6"]
                 [stigmergy/ikota "0.1.0-SNAPSHOT"]]
  :plugins [[refactor-nrepl "2.4.0"]]
  :jvm-opts ["-server" "-Dconf=.lein-env"]
  :source-paths ["src/clj" "src/cljc"]
  :resource-paths ["resources"]
  :uberjar-name "chp.jar"
  :main stigmergy.server
  :clean-targets ^{:protect false}  [:target-path
                                     [:cljsbuild :builds :app :compiler :output-dir]
                                     [:cljsbuild :builds :app :compiler :output-to]]

  :profiles {:dev {:source-paths ["env/dev/clj"]
                   :resource-paths ["env/dev/resources"]
                   :repl-options {:init-ns user
                                  :timeout 120000}}
             :uberjar {:source-paths ["env/prod/clj"]
                       :resource-paths ["env/prod/resources"]
                       :prep-tasks ["compile"]
                       :aot :all}})
