(ns cpress.client
  (:require [reagent.core :as r]))

(defn with-key [cols]
  (for [[index col] (map-indexed (fn [index element]
                                   [index element]) cols)]
    (with-meta col {:key index})))

(defn card [item]
  [:div {:class "mdl-card mdl-shadow--2dp"
         :style {:margin-left :auto
                 :margin-right :auto
                 :margin-top 10
                 :margin-bottom 10
                 :width "70%"}}
   [:div {:class "mdl-card__title mdl-card--expand"
          :style {:background-image "url(/img/coffee-cup.jpg"
                  :background-repeat :no-repeat
                  :background-size :cover
                  :height 200}}]
   [:div
    [:div {:class "mdl-card__supporting-text"
           :style {:display :inline}} (:item/name item)]
    [:button {:class "mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
              :style {:float :right}
              :on-click #(js/alert "add")}
     [:i {:class "material-icons"} "add"]]]])

(def app [:div {:class "demo-blog mdl-layout mdl-js-layout"
                :style {:background-image "url('/img/coffee-bean.jpg')"}}
          [:main {:class "mdl-layout__content"}
           (with-key  (for [i (range 4)]
                        [card {:item/name (str "cafe late " i)}]))]
          [:button {:class "mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
                    :style {:position :fixed
                            :top 0
                            :right 0
                            :z-index 10
                            :margin-right 10}
                    :on-click #(js/alert "add")}
           [:i {:class "material-icons"} "shopping_cart"]]])

(r/render app (js/document.getElementById "app"))

