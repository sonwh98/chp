(ns stigmergy.server
  (:require [org.httpkit.server :as httpkit]
            [stigmergy.chp]
            [stigmergy.config :as c]
            [ring.middleware.params]
            [ring.middleware.file]
            [ring.middleware.content-type]
            [bidi.ring :as bidi] :reload)
  (:gen-class))

(def server (atom nil))

(defn create-app []
  (c/reload)
  (let [routes (c/config :bidi-routes)
        handler (bidi/make-handler routes)
        mime-types (merge {"chp" "text/html"
                           nil "text/html"}
                          (c/config :mime-types))
        app (-> handler
                (ring.middleware.file/wrap-file (or (c/config :public-dir) "."))
                ring.middleware.params/wrap-params
                (ring.middleware.content-type/wrap-content-type {:mime-types mime-types}))]
    app))

(defn start-http []
  (let [app (create-app)
        port (or (c/config :port) 3000)]
    (prn "listening on port " port)
    (reset! server (httpkit/run-server app {:port port}))))

(defn stop-http []
  (when @server
    (@server :timeout 100)
    (reset! server nil)))

(defn -main [& args]
  ;;java -Dconf=config.clj -jar target/chp.jar
  ;;java -Dconf=../artoflambda/config.clj -jar target/chp.jar
  (prn "starting....")
  (start-http)
  (prn "started"))

(defn restart []
  (stop-http)
  (start-http))

(comment
  (System/getProperty "config")
  (System/setProperty "config" "../lambdakids/config.clj")
  (require '[stigmergy.config :as cf] :reload-all)
  stigmergy.config/config
  (start-http))
