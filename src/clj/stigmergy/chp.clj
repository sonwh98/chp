(ns stigmergy.chp
  (:require  [ring.util.response :as rur]
             [clojure.walk :as cw]
             [stigmergy.config :as c]
             [garden.core]
             [stigmergy.ikota :as ik]))

(defn- rename-include
  "a chp can call include to include other chp pages.
  the include function is in the stigmergy.chp namespace so must rewrite include to stigmergy.chp/include"
  [hiccup]
  (cw/postwalk (fn [this-symbol]
                 (if (= this-symbol 'include)
                   'stigmergy.chp/include
                   this-symbol))
               hiccup))

(defn chp-eval
  "evaluates the chp file and return the hiccup as a data structure. this implementation uses load-file instead of eval"
  [chp-file bindings]
  (declare ^:dynamic symbol-table)
  (declare ^:dynamic request)
  (declare ^:dynamic params)
  (declare ^:dynamic css)
  (binding [*ns* (find-ns 'stigmergy.chp)
            request (:request bindings)
            params (-> bindings :request :params)
            symbol-table bindings
            css garden.core/css]
    (let [hiccup-data (load-file chp-file)
          hiccup-data (if (vector? hiccup-data)
                        (-> hiccup-data rename-include)
                        [:div (str hiccup-data)])]
      hiccup-data)))

(defn include
  ([chp bindings]
   (let [chp-dir (or (c/config :chp-dir) ".")
         chp-file (str chp-dir "/" chp)]
     (chp-eval chp-file bindings)))

  ([chp]
   (include chp {})))

(defn render
  "bindings is a map of symbols"
  ([chp bindings]
   (let [hiccup-data (include chp bindings)
         hiccup-str (ik/hiccup->html-str hiccup-data)
         html5 (format "<!DOCTYPE html>\n%s" hiccup-str)]
     (rur/response html5)))
  ([chp]
   (render chp {})))

(defn hiccup-page-handler [req]
  (let [chp (:uri req)]
    (render chp {:request req})))
