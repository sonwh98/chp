(ns stigmergy.config)

(defn load-config []
  (let [config-file (or (System/getProperty "config") "./config.clj")]
    (try
      (load-file config-file)
      (catch Exception e (.printStackTrace e)))))

(defn reload []
  (def config (load-config)))
