(def public-dir "../lambdakids/public")

(defn read-blog-entries []
  (let [descending #(* -1 (compare %1 %2))
        blog-dir (str public-dir "/blog-entries")]
    (for [blog-entry (->> blog-dir clojure.java.io/file
                          file-seq
                          rest
                          (remove #(or (re-find #"~" (.getName %))
                                       (re-find #"#" (.getName %))))
                          ;;(map #(-> % slurp read-string eval (assoc :blog/file-name (.getName %))))
                          (map #(-> (.getCanonicalPath %) load-file (assoc :blog/file-name (.getName %))))
                          (sort-by :blog/date descending))]
      blog-entry)))

(def template "template.chp")

(defn get-all-entries []
  (let [all-entries (read-blog-entries)
        entries-per-page 2
        blog-entries-partition-by-3 (->> all-entries
                                         (partition-all entries-per-page))
        total-pages (/ (count all-entries)
                       entries-per-page)]
    [all-entries total-pages]))

(defn blog-handler [req]
  (let [file-name (-> req :params :file-name)
        [all-entries total-pages] (get-all-entries)
        blog-entries (filter #(= file-name (:blog/file-name %))
                             all-entries)]
    (if (= file-name "clojure.min.js.map")
      {} ;;hack is neccessary because klipse tries to load clojure.min.js.map
      (stigmergy.chp/render template {:visible-blogs blog-entries
                                      :all-blogs (read-blog-entries)
                                      :total-pages total-pages}))))

(defn page-handler [req]
  (let [{{:keys [page-num]} :params} req
        page-num (Integer/parseInt page-num)
        [all-entries total-pages] (get-all-entries)
        entries-per-page 2
        blog-entries-partition-by-3 (->> all-entries
                                         (partition-all entries-per-page))
        visible-blogs (nth blog-entries-partition-by-3 page-num)]
    (stigmergy.chp/render template {:visible-blogs visible-blogs
                                    :page-num page-num
                                    :all-blogs all-entries
                                    :total-pages total-pages})))

(defn txt-exists-in? [file-name txt]
  (let [content (slurp file-name)
        content (.toLowerCase content)
        txt (.toLowerCase txt)]
    (.contains content txt)
    ))

(defn search [txt]
  ;;read all files in public/blog-entries
  (let [blog-files (rest (file-seq (clojure.java.io/file (str public-dir "/blog-entries"))))
        file-existence(for [f blog-files]
                        (let [file-name (.getAbsolutePath f)
                              exists? (txt-exists-in? file-name txt)]
                          [file-name exists?]))
        txt-exist? (fn [entry]
                     (second entry))
        files-boolean-pair (filter txt-exist? file-existence)]
    (for [p files-boolean-pair]
      (let [file-name (first p)]
        file-name))
    )
  )

(defn search-handler [req]
  (let [blog-entries (read-blog-entries)
        params (:params req)
        search-txt (params "search-txt")
        search-result (search search-txt)]
    
    (stigmergy.chp/render template {:all-blogs blog-entries
                                    :search-result search-result}
                          )))

(defn edit-handler [req]
  (let [params (:params req)
        file-name (params "file-name")
        ]
    (prn file-name)
    (stigmergy.chp/render "edit.chp" {:file-name file-name})))

(defn save-handler [req]
  (let [params (:params req)
        file-name (params "file-name")
        updated-blog-content (params "blog-content")
        full-file-path (str public-dir "/blog-entries/" file-name)]
    (spit full-file-path updated-blog-content)
    (prn updated-blog-content))
  (prn "The saving function is working")
  (stigmergy.chp/render "save.chp"))

{:environment :production
 :port 3000
 :chp-dir public-dir
 :public-dir public-dir
 :index template
 :mime-types {"chp" "text/html" 
              "blog" "text/html"
              nil "text/html"}
 :bidi-routes ["/" [
                    ["" (fn [req]
                          (page-handler {:params {:page-num "0"}}))]
                    [[[#"\d+" :page-num]] page-handler]
                    [[[#"\d+" :year] "/"
                      [#"\d+" :month] "/"
                      [#"\d+" :day] "/"
                      [#".*" :file-name]] blog-handler]
                    [#".*\.chp"  stigmergy.chp/hiccup-page-handler]
                    ["search" search-handler]
                    ["edit" edit-handler]
                    ["save" save-handler]
                    ]]}

