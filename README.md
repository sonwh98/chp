# Clojure Hiccup Page (CHP)

A Clojure DSL inspired by PHP for server-side web development.

## Rationale
PHP is popular for server-side web development because it is easy to get started. Take any .html file, rename it
with a .php extension, add some conditionals, loops and database calls and you have a working dynamic site. This is
easy and simple.

PHP was initially designed to be a templating language to create dynamic HTML pages on the server-side.
PHP pages are HTML pages with embedded code. CHP borrows this idea from PHP but improves on it.
Instead of HTML and PHP code, CHP uses [Hiccup](https://bitbucket.org/sonwh98/ikota/src/master) and Clojure code.

Hiccup is HTML represented as plain Clojure data structures. Using data structures to represent HTML is better than
using HTML strings because it gives the programmer the full power of Clojure to construct, transform and
manipulate data structures.

Unlike PHP, CHP does not mix two different languages. For example, take a look at the following PHP code
to generate an html list

```PHP
<ul style="color: red">
 <?php foreach( range(0, 3) as $i): ?>
       <li> <?php echo $i ?> </li>
 <?php endforeach; ?> 
</ul>
```

The php code has to be escaped with <?php ... ?> so that the parser knows what is php code and
what is html markup. The <?php ... ?> escape hatch is tedious to type and adds unnecessary noise making the
code harder to read.

Now contrast that with CHP

```Clojure
[:ul {:style {:color :red}} 
  (for [i (range 3)]       
    [:li i])]
```

CHP is not only shorter and more concise than PHP but there is no mixing of two different languages
and thus no mental context switching between two different syntax and language. Everything is simply a Clojure
data structure or evaluates to a Clojure data structure.

### CSS
CSS is generated using [Garden](https://github.com/noprompt/garden). Use the css function with Garden syntax 
to generate CSS strings.

For example, this will give a red background-color
to all div elements:

```Clojure
[:style 
    (css [:div {:background-color :red}])]
```


### Quick Start

```bash
% lein uberjar
% java -Dconfig=app.clj -jar target/chp.jar
```

open url http://localhost:3000/index.chp

app.clj defines various keys to configure the app. For example,
```clojure
{:port 3000
 :chp-dir "resources/chp" ;;where to find CHP files
 :bidi-routes ["/" [[#".*\.chp"  stigmergy.chp/hiccup-page-handler]]]}

```

Put CHP files in the directory specfied in :chp-dir (in this case resources/chp) and it will
be served to web browsers.

### Example
Look in resources/chp for more examples.
For a complete example of a blog implemented using CHP, checkout [Lambda Kids](https://bitbucket.org/sonwh98/lambdakids/src/master/)
## License

Copyright © 2017 Sun Tzu

Distributed under the Eclipse Public License either version 1.0
