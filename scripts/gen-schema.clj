#!/usr/bin/env boot

;;usage: ./gen-schema.clj > ../resources/schema.edn

(set-env! :repositories #(conj % ["my.datomic.com" {:url      "https://my.datomic.com/repo"
                                                    :username "son.c.to@gmail.com"
                                                    :password "5884b70b-fdbc-4bc0-97c6-26b66e138641"}]))

(set-env!
 :source-paths #{"../src/clj"}
 :dependencies '[[org.clojure/clojure "1.8.0"]
                 [com.datomic/datomic-pro "0.9.5561.50" :exclusions [joda-time guava]]
                 [datomic-schema "1.3.0"]])

(require '[datomic-schema.schema :as s]
         '[cpress.schema :as cpress])



(->> cpress/schema s/generate-schema
     (assoc {} :datomic-schema)
     pr-str
     println)




