(ns user
  (:require [stigmergy.server :as s]))

(defn start []
  (s/start-http))

(defn stop []
  (s/stop-http))

(defn restart []
  (stop)
  (start))

(comment
  (System/getProperty "config")
  (System/setProperty "config" "lambdakids.clj")
  (System/setProperty "config" "app.clj")
  
  (declare ^:dynamic symbol-table)
  (binding [symbol-table {:person/name "Sonny"}
            *ns* (create-ns 'foo)]
    (let [v (load-file "resources/hiccup/foo.clj")]
      (prn "v=" v))
    )

  (binding [symbol-table {:person/name "Sonny"}
            *ns* (create-ns 'foo)]
    (let [v (-> "hiccup/foo.clj" clojure.java.io/resource .toString clojure.java.io/reader load)]
      (prn "v=" v))
    )

  (-> "hiccup/foo.clj" clojure.java.io/resource .toString)
  
  (binding [symbol-table {:person/name "Sonny"}
            *ns* (create-ns 'foo)]
    (let [v (-> "hiccup/foo.clj" clojure.java.io/resource slurp read-string eval)]
      (prn "v=" v)))

  (require '[garden.core :refer [css]])
  (css [:body {:font-size "16px"}])
  )
