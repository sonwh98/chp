(defn blog-handler [req]
  (let [{{:keys [year month day title]} :params} req]
    (stigmergy.chp/render title)))

{:environment :dev
 :port 3000
 :chp-dir "resources/chp"
 :public-dir "resources/public"
 :index "quickstart.chp"
 :mime-types {"chp" "text/html"
              "blog" "text/html"
              nil "text/html"}
 :bidi-routes ["/" [
                    ["" (fn [req]
                          (let [index (or (stigmergy.config/config :index) "index.chp")]
                            (stigmergy.chp/render index {:request req})))]

                    [[[#"\d+" :year] "/"
                      [#"\d+" :month] "/"
                      [#"\d+" :day] "/"
                      [#".*" :title]] blog-handler]

                    [#".*\.chp"  stigmergy.chp/hiccup-page-handler]
                    ]]
 }

