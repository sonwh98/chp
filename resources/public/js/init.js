var drawerEl = document.querySelector('.mdc-drawer');
console.log(drawerEl)
var MDCTemporaryDrawer = mdc.drawer.MDCTemporaryDrawer;
var drawer = new MDCTemporaryDrawer(drawerEl);
document.querySelector('.demo-menu').addEventListener('click', function() {
    drawer.open = true;
});
drawerEl.addEventListener('MDCTemporaryDrawer:open', function() {
    console.log('Received MDCTemporaryDrawer:open');
});
drawerEl.addEventListener('MDCTemporaryDrawer:close', function() {
    console.log('Received MDCTemporaryDrawer:close');
});

// Demonstrate application of --activated modifier to drawer menu items
var activatedClass = 'mdc-list-item--selected';
document.querySelector('.mdc-drawer__drawer').addEventListener('click', function(event) {
    var el = event.target;
    while (el && !el.classList.contains('mdc-list-item')) {
	el = el.parentElement;
    }
    if (el) {
	var activatedItem = document.querySelector('.' + activatedClass);
	if (activatedItem) {
	    activatedItem.classList.remove(activatedClass);
	}
	event.target.classList.add(activatedClass);
    }
});

var radioEl = document.querySelector('#demo-radio-buttons');
radioEl.addEventListener('change', function(e) {
    drawerEl.classList.remove('demo-drawer--custom');
    drawerEl.classList.remove('demo-drawer--accessible');

    if(e.target.id === 'theme-radio-accessible') {
	drawerEl.classList.add('demo-drawer--accessible');
    } else if(e.target.id === 'theme-radio-custom') {
	drawerEl.classList.add('demo-drawer--custom');
    }
});

var rtlToggleBtn = document.querySelector('.demo-toolbar-example-heading__rtl-toggle-button');
rtlToggleBtn.addEventListener('click', function(event) {
    document.body.setAttribute('dir', document.body.getAttribute('dir') === 'rtl' ? 'ltr' : 'rtl');
});

